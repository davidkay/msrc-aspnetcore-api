﻿using System;
using System.IO;
using System.Threading.Tasks;
using Msrc.Infrastructure;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace Msrc.IntegrationTests
{
    public class SliceFixture : IDisposable
    {
        private readonly IServiceScopeFactory _scopeFactory;

        private readonly string DbName = Guid.NewGuid() + ".db";

        public SliceFixture()
        {
            var startup = new Startup();
            var services = new ServiceCollection();

            services.AddSingleton(new MsrcContext(DbName));

            startup.ConfigureServices(services);

            var provider = services.BuildServiceProvider();


            provider.GetRequiredService<MsrcContext>().Database.EnsureCreated();
            _scopeFactory = provider.GetService<IServiceScopeFactory>();
        }

        public void Dispose()
        {
            File.Delete(DbName);
        }

        public async Task ExecuteScopeAsync(Func<IServiceProvider, Task> action)
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                await action(scope.ServiceProvider);
            }
        }

        public async Task<T> ExecuteScopeAsync<T>(Func<IServiceProvider, Task<T>> action)
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                return await action(scope.ServiceProvider);
            }
        }

        public Task<TResponse> SendAsync<TResponse>(IRequest<TResponse> request)
        {
            return ExecuteScopeAsync(sp =>
            {
                var mediator = sp.GetService<IMediator>();

                return mediator.Send(request);
            });
        }

        public Task SendAsync(IRequest request)
        {
            return ExecuteScopeAsync(sp =>
            {
                var mediator = sp.GetService<IMediator>();

                return mediator.Send(request);
            });
        }

        public Task ExecuteDbContextAsync(Func<MsrcContext, Task> action)
        {
            return ExecuteScopeAsync(sp => action(sp.GetService<MsrcContext>()));
        }

        public Task<T> ExecuteDbContextAsync<T>(Func<MsrcContext, Task<T>> action)
        {
            return ExecuteScopeAsync(sp => action(sp.GetService<MsrcContext>()));
        }

        public Task InsertAsync(params object[] entities)
        {
            return ExecuteDbContextAsync(db =>
            {
                foreach (var entity in entities)
                {
                    db.Add(entity);
                }
                return db.SaveChangesAsync();
            });
        }
    }
}