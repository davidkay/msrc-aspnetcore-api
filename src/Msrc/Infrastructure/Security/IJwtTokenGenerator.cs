﻿using System.Threading.Tasks;

namespace Msrc.Infrastructure.Security
{
    public interface IJwtTokenGenerator
    {
        Task<string> CreateToken(string username);
    }
}