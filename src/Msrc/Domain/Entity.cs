using System.Collections.Generic;
using Newtonsoft.Json;

namespace Msrc.Domain
{
    public class Entity
    {
        [JsonIgnore]
        public string EntityId { get; set; }
        public string EntityDisplayName { get; set; }
        public string ParentEntityId { get; set; }
        public string ParentEntityName { get; set; }
        public string EntityType { get; set; }
        public string EntityGrade { get; set; }
        public string EntityAccountabilityScore { get; set; }
        public string EntityAddress { get; set; }
        public string EntityCity { get; set; }
        public string EntityState { get; set; }
        public string EntityZip { get; set; }
        public string EntityContactName { get; set; }
        public string EntityContactEmail { get; set; }
        public string EntityLevel { get; set; }
        public string EntityTSICSI { get; set; }
        public string EntityTSICSIDesc { get; set; }
        public string EntityGradeWithEL { get; set; }
        
        [JsonIgnore]
        public List<EntityComponents> EntityComponents { get; set; }
    }
}