namespace Msrc.Domain
{
    public class EntityComponents
    {
        public string EntityId { get; set; }
        public Component Component { get; set; }
        public Entity Entity { get; set; }
    }
}