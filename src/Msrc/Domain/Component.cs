using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Newtonsoft.Json;

namespace Msrc.Domain
{
    public class Component
    {
        [JsonIgnore]
        public string EntityId { get; set; }
        public string ComponentName { get; set; }
        public string ComponentCode { get; set; }
        public string AggregationCode { get; set; }
        public string ComponentLevel { get; set; }
        public float ComponentValue { get; set; }
        public float ComponentActualValue { get; set; }
        public int ComponentNCount { get; set; }
        public int ArticleId { get; set; }

        // [NotMapped]
        //public bool Favorited => ArticleFavorites?.Any() ?? false;
        //
        //[NotMapped]
        //public int FavoritesCount => ArticleFavorites?.Count ?? 0;
        //
        //[NotMapped]
        //public List<string> TagList => (ArticleTags?.Select(x => x.TagId) ?? Enumerable.Empty<string>()).ToList();
        //
        //[JsonIgnore]
        //public List<ArticleTag> ArticleTags { get; set; }
        //
        //[JsonIgnore]
        //public List<ArticleFavorite> ArticleFavorites { get; set; }
        //
        //public DateTime CreatedAt { get; set; }
        //
        //public DateTime UpdatedAt { get; set; }
    }
}