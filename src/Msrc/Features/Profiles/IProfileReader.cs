﻿using System.Threading.Tasks;

namespace Msrc.Features.Profiles
{
    public interface IProfileReader
    {
        Task<ProfileEnvelope> ReadProfile(string username);
    }
}