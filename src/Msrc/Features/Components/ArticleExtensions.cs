using System.Linq;
using Msrc.Domain;
using Microsoft.EntityFrameworkCore;

namespace Msrc.Features.Components
{
    public static class ComponentExtensions
    {
        public static IQueryable<Component> GetAllData(this DbSet<Component> Components)
        {
            return components
                .Include(x => x.Author)
                .Include(x => x.ArticleFavorites)
                .Include(x => x.ArticleTags)
                .AsNoTracking();
        }
    }
}