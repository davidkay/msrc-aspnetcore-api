using Msrc.Domain;

namespace Msrct.Features.Components
{
    public class ComponentEnvelope
    {
        public ComponentEnvelope(Component component)
        {
            Component = component;
        }

        public Component Component { get; }
    }
}