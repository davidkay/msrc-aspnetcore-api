using System.Collections.Generic;

namespace Msrc.Features.Tags
{
    public class TagsEnvelope
    {
        public List<string> Tags { get; set; }
    }
}